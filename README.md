# Wardrobify

Team:

- Adrian Ma - Shoes
- Sirasit Punnanithi - Hats

## Design

## Shoes microservice

Created two models: BinVO and Shoe. Shoe has the attributes manufacturer, model_name, color, picture_url, and bin. bin is a foreign key to our BinVO model because 1 bin can have many shoes. BinVO has the same attributes as the Bin model within our wardrobe
models.py.
Made RESTful API view functions that allows the shoe microservice to exchange data/communicate with our wardrobe microservice. setup our urls.py to define URL patters to map our views and also our poller which periodically checks for changes and updates the BinVO in our database.
Utilized React to create our frontend and display the shoe data. created shoelist for displaying a list of shoes and which bin it is located in, created createshoe for that allows us to create new shoes, created a shoedetail that displays manufacturer, color, model name, picture url, bin.
Delete functionality was implimented in the shoe detail page and will redirect you back to shoe list page once successful.
Selecting the hyperlink/manufacturer in the shoe list page will redirect you to that specific detail page and provide you more information include an image of the shoe.
Create shoe functionality was implimented in the shoe list page and will redirect you to the create new shoe form. form will clear once created to allow user to create multiple shoes at onces.
Updated the apps.js file and used the react router dom to setup routing logic to redirect users to their desired page by using the URL paths we provided.

http://localhost:3000/shoes - shoelist
http://localhost:3000/shoes/new - createshoe-form (page/create button)
http://localhost:3000/api/shoe/1/ - detailshoe-form (page/delete button)

## Hats microservice

The Hats microservice aims to establish a scalable and efficient backend with Django and store data using PostgresSQL database. Users can expect to dependably view, create and delete instances of the Hat model within the Hat microservice. The Hat model uses properties such as the name, brand, picture, created date, updated date and location.

RESTful API was used to poll the Wardrobe microservice to obtain Location data so the Hats microservice can store the data within the Location ValueObject(VO) model. Therefore, data from Location within Wardrobe microservice is not directly created or modified. Additionally, RESTful API principles are used to by React to display, create and delete data from the Hat microservice and modify data within the Hat backend and database.

The React pages for the Hat microservice aims to display a list of the hats within a user's collection, display information about one hat within the collection, create a new hat for the collection and delete a hat from the collection. The create functionality was implemented using a simple form where the user can input some information pertaining to the hat. All of the changes will be reflected on the page in realtime.

URLs needed to access React pages:
http://localhost:3000 - Main Wardrobify page
http://localhost:3000/hats - List of hats page
http://localhost:3000/hats/new - Create a new hat page
http://localhost:3000/hats/<<id>> - Detail page of a hat with added delete functionality
