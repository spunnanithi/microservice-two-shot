from django.db import models
from django.urls import reverse

# Create your models here.


# had to do null=True because i made adjustment to model after there was bins created.
# attempted to wipe database but it would not work

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True,blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True,blank=True)

    def __str__(self):
        return self.closet_name



class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return self.model_name