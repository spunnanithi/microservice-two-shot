# Generated by Django 4.0.3 on 2023-03-02 23:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_hat_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationvo',
            name='id',
            field=models.PositiveIntegerField(primary_key=True, serialize=False),
        ),
    ]
