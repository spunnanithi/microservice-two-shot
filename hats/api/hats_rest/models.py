from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    id = models.PositiveIntegerField(primary_key=True, null=False)
    section_number = models.PositiveIntegerField(null=True)
    shelf_number = models.PositiveIntegerField(null=True)


class Hat(models.Model):
    name = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
        default="",
    )

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
