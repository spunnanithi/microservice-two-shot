import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatList from "./HatList";
import HatDetail from "./HatDetail";
import ShoeList from "./ShoeList";
import HatCreateForm from "./HatCreateForm";
import ShoeCreateForm from "./ShoeCreateForm";
import ShoeDetail from "./ShoeDetail";



function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="hats">
						<Route path="" element={<HatList />}></Route>
						<Route path=":id" element={<HatDetail />}></Route>
						<Route path="new" element={<HatCreateForm />}></Route>
					</Route>
					<Route path="shoes">
						<Route path="" element={<ShoeList/>}></Route>
						<Route path="new" element={<ShoeCreateForm/>}></Route>
					</Route>
					<Route path="api/shoe">
						<Route path=":id" element={<ShoeDetail/>}></Route>
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
