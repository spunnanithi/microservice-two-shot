import React from "react";
import {useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

const ShoeDetail = () => {
	const [shoes, setShoes] = useState([]);
	const location = useLocation();
	console.log(location, "this is location")
	const shoe = location.state.myState.shoe;
	console.log(shoe, "this is shoeeeee")
		const fetchShoes = async () => {
			const shoeUrl = `http://localhost:8080${shoe.href}`;
			console.log(shoeUrl, "this is shoeURLLLLLLLL")
			const response = await fetch(shoeUrl);

			if (response.ok) {
				const data = await response.json();
			console.log(data, " THIS is the dataaaaaaa")
				setShoes(data.shoes);
			} else {
				console.error(response);
			}
		};

	const navigate = useNavigate();

	const handleDelete = async (event) => {
			event.preventDefault();
			const shoeUrl = `http://localhost:8080${shoe.href}`;
			console.log(shoeUrl);

			const fetchConfig = {
				method: "DELETE",
			};

			const response = await fetch(shoeUrl, fetchConfig);

			if (response.ok) {
				navigate('/shoes', {replace: true});
				console.log("Successfully deleted shoe.");
			}
		};

		useEffect(() => {
			fetchShoes();
		}, []);


		return (
			<div className="container">
				<div className="d-flex mt-5 justify-content-center">
				<div className="card" style={{ width: "40vh" }}>
			<img
				className="card-img-top"
				src={shoe.picture_url}
				alt="Card image cap"
				style={{
				maxHeight: "40vh",
				maxWidth: "40vw",
				}}></img>
			<div className="card-body">
				<h5 className="card-title">{shoe.manufacturer}</h5>
				<p className="card-text">
				Model: {shoe.model_name} <br></br> Color: {shoe.color} <br></br> Bin: {shoe.bin.closet_name}
				</p>
				<a className="btn btn-primary" onClick={handleDelete}>
				Delete
				</a>
			</div>
			<div className="card-footer">
				PictureUrl: {shoe.picture_url}
			</div>
					</div>
				</div>
			</div>
		);
	}

export default ShoeDetail;