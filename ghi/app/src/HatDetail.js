import React from "react";
import { useParams, useLocation, useNavigate } from "react-router-dom";
import { useState } from "react";

const HatDetail = () => {
	const { id } = useParams();
	const location = useLocation();
	const hat = location.state.myState.hat;
	const created_date = new Date(hat.created).toLocaleDateString("en-US");
	const updated_date = new Date(hat.updated).toLocaleDateString("en-US");

	const navigate = useNavigate();
	const handleDelete = async (event) => {
		event.preventDefault();
		const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`;
		console.log(hatUrl);

		const fetchConfig = {
			method: "DELETE",
		};

		const response = await fetch(hatUrl, fetchConfig);

		if (response.ok) {
			navigate("/hats", { replace: true });
			console.log("Successfully deleted hat.");
		}
	};

	return (
		<div className="container">
			<div className="d-flex mt-5 justify-content-center">
				<div className="card" style={{ width: "40vh" }}>
					<img
						className="card-img-top"
						src={hat.picture_url}
						alt="Card image cap"
						style={{
							maxHeight: "40vh",
							maxWidth: "40vw",
						}}></img>
					<div className="card-body">
						<h5 className="card-title">{hat.name}</h5>
						<p className="card-text">
							{hat.brand} <br></br> {hat.location}
						</p>
						<a className="btn btn-primary" onClick={handleDelete}>
							Delete
						</a>
					</div>
					<div className="card-footer">
						Created on: {created_date} <br></br>
						Updated on: {updated_date}
					</div>
				</div>
			</div>
		</div>
	);
};

export default HatDetail;
