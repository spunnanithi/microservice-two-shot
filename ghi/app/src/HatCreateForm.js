import React from "react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const HatCreateForm = () => {
	const navigate = useNavigate();

	const [locations, setLocations] = useState([]);

	const [name, setName] = useState("");
	const [brand, setBrand] = useState("");
	const [picture_url, setPictureUrl] = useState("");
	const [location, setLocation] = useState("");

	const handleFormChange = (event) => {
		const value = event.target.value;
		const eleName = event.target.name;

		if (eleName === "name") {
			setName(value);
		} else if (eleName === "brand") {
			setBrand(value);
		} else if (eleName === "picture_url") {
			setPictureUrl(value);
		} else if (eleName === "location") {
			setLocation(value);
		}
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			name: name,
			brand: brand,
			picture_url: picture_url,
			location: location,
		};

		const hatUrl = "http://localhost:8090/api/hats/";
		const fetchConfig = {
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(hatUrl, fetchConfig);

		if (response.ok) {
			const newHat = await response.json();
			console.log(newHat);

			setName("");
			setBrand("");
			setPictureUrl("");
			setLocation("");
		}
	};

	const fetchData = async () => {
		const locationUrl = "http://localhost:8100/api/locations/";

		const response = await fetch(locationUrl);

		if (response.ok) {
			const data = await response.json();
			setLocations(data.locations);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a New Hat Entry</h1>
					<form onSubmit={handleSubmit}>
						<div className="mb-3">
							<label htmlFor="name" className="form-label">
								Name
							</label>
							<input
								type="text"
								className="form-control"
								id="name"
								name="name"
								placeholder="Please enter the hat name"
								required
								onChange={handleFormChange}
								value={name}></input>
						</div>
						<div className="mb-3">
							<label htmlFor="brand" className="form-label">
								Brand
							</label>
							<input
								type="text"
								className="form-control"
								id="brand"
								name="brand"
								placeholder="Please enter hat brand"
								required
								onChange={handleFormChange}
								value={brand}></input>
						</div>
						<div className="mb-3">
							<label htmlFor="picture_url" className="form-label">
								Picture URL
							</label>
							<input
								type="url"
								className="form-control"
								id="picture_url"
								name="picture_url"
								placeholder="Please enter valid picture url"
								required
								onChange={handleFormChange}
								value={picture_url}></input>
						</div>
						<div className="mb-3">
							<select
								id="location"
								name="location"
								className="form-select"
								required
								onChange={handleFormChange}
								value={location}>
								<option value="">Choose a location</option>
								{locations.map((location) => {
									return (
										<option key={location.id} value={location.href}>
											{location.closet_name}
										</option>
									);
								})}
							</select>
						</div>
						<button type="submit" className="btn btn-primary">
							Create
						</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default HatCreateForm;
