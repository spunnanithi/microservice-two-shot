import React from "react";
import { useEffect, useState } from "react";
import { NavLink, Outlet } from "react-router-dom";

const HatList = () => {
	const [hats, setHats] = useState([]);

	const fetchHats = async () => {
		const hatUrl = "http://localhost:8090/api/hats/";

		const response = await fetch(hatUrl);

		if (response.ok) {
			const data = await response.json();
			setHats(data.hats);
		} else {
			console.error(response);
		}
	};

	useEffect(() => {
		fetchHats();
	}, []);

	return (
		<>
			<div className="d-flex mt-5 mb-5 justify-content-center">
				<NavLink className={"btn btn-primary"} to={"/hats/new"}>
					Create a New Hat
				</NavLink>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Location</th>
					</tr>
				</thead>
				<tbody>
					{hats.map((hat) => {
						return (
							<tr key={hat.id}>
								<td>
									<NavLink to={`/hats/${hat.id}`} state={{ myState: { hat } }}>
										{hat.name}
									</NavLink>
								</td>
								<td>{hat.location}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<Outlet />
		</>
	);
};

export default HatList;
