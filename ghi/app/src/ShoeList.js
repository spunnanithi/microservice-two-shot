import React from "react";
import { useEffect, useState } from "react";
import { NavLink, Outlet } from "react-router-dom";

const ShoeList = () => {
	const [shoes, setShoes] = useState([]);

	const fetchShoes = async () => {
		const shoeUrl = "http://localhost:8080/api/shoe/";

		const response = await fetch(shoeUrl);

		if (response.ok) {
			const data = await response.json();
			setShoes(data.shoes);
		} else {
			console.error(response);
		}
	};

	useEffect(() => {
		fetchShoes();
	}, []);

	return (
		<>
			<div className="d-flex mt-5 mb-5 justify-content-center">
				<NavLink className={"btn btn-primary"} to={"/shoes/new"}>
					Create a New Shoe
				</NavLink>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Manufacturer</th>
						<th>Bin</th>
					</tr>
				</thead>
				<tbody>
					{shoes.map((shoe) => {
						return (
							<tr key={shoe.href}>
								<td>
									<NavLink to={`${shoe.href}`} state={{ myState: { shoe } }}>
										{shoe.manufacturer}
									</NavLink>
								</td>
								<td>{shoe.bin.closet_name}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<Outlet />
		</>
	);
};

export default ShoeList;